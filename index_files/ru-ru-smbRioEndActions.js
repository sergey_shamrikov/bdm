/*
RIO EA tagging for Business
- based on WEDCS solution for Enterprise site
contacts: v-lukrin@microsoft.com; v-lholy@microsoft.com
*/

$.fn.eaDoTracking = function (tracking) {
	this.each(function () {
		$(this).attr(tracking);
	});
};

$(document).ready(function () {
	var eaUrl = window.location.pathname;
	eaStartTracking();

	function eaStartTracking() {
		//if (eaUrl.toLowerCase().indexOf("/ru-ru/business/") >= 0) {
		//    $("a[href*='http://www.microsoft.com/ru-ru/smb/campaigns/Get-Modern/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300148611, '');" });
		//    $("a[href*='http://www.microsoft.com/ru-ru/smb/campaigns/New-Office-For-Business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300148612, '');" });

		//}


		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/") >= 0) {
		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/retiring-xp.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300152572, '');" });


		    $("a[href*='http://windows.microsoft.com/ru-ru/windows-8/meet']").eaDoTracking({ "onclick": "return RioTracking.click(300217412, '');" });
		    $("a[href*='http://microsoftstore.com/']").eaDoTracking({ "onclick": "return RioTracking.click(300217413, '');" });
		    $("a[href*='http://office.microsoft.com/ru-RU#product']").eaDoTracking({ "onclick": "return RioTracking.click(300217414, '');" });
		    $("a[href*='http://www.windowsazure.com/ru-ru/']").eaDoTracking({ "onclick": "return RioTracking.click(300217415, '');" });
		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/crm.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300217416, '');" });
		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300217417, '');" });
		    $("a[href*='http://www.microsoft.com/sqlserver/ru/ru/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300217431, '');" });
		    $("a[href*='http://www.skype.com/ru/']").eaDoTracking({ "onclick": "return RioTracking.click(300217418, '');" });
		    $("a[href*='http://office.microsoft.com/ru-RU#office']").eaDoTracking({ "onclick": "return RioTracking.click(300217419, '');" });
		    $("a[href*='http://office.microsoft.com/ru-RU#office365']").eaDoTracking({ "onclick": "return RioTracking.click(300217414, '');" });
		    $("a[href*='http://www.office.microsoft.com/ru-ru/exchange/']").eaDoTracking({ "onclick": "return RioTracking.click(300217420, '');" });
		    $("a[href*='http://office.microsoft.com/ru-ru/lync/']").eaDoTracking({ "onclick": "return RioTracking.click(300217421, '');" });
		    $("a[href*='http://office.microsoft.com/sharepoint/']").eaDoTracking({ "onclick": "return RioTracking.click(300217422, '');" });
		    $("a[href*='http://office.microsoft.com/project']").eaDoTracking({ "onclick": "return RioTracking.click(300217423, '');" });
		    $("a[href*='http://office.microsoft.com/visio']").eaDoTracking({ "onclick": "return RioTracking.click(300217424, '');" });
		    $("a[href*='http://windows.microsoft.com/ru-ru/internet-explorer/download-ie-MCM']").eaDoTracking({ "onclick": "return RioTracking.click(300217425, '');" });
		    $("a[href*='http://advertise.bingads.microsoft.com/']").eaDoTracking({ "onclick": "return RioTracking.click(300217426, '');" });
		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/erp.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300217427, '');" });
		    $("a[href*='http://windows.microsoft.com/ru-ru/windows/security-essentials-download/']").eaDoTracking({ "onclick": "return RioTracking.click(300217428, '');" });
		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-intune/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300217429, '');" });
		    $("a[href*='http://microsoft.com/systemcenter']").eaDoTracking({ "onclick": "return RioTracking.click(300217430, '');" });


		    $("a[href*='']").eaDoTracking({ "onclick": "return RioTracking.click(, '');" });

		}

		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/how-to-buy/") >= 0) {

		    $("a[href*='http://office.microsoft.com/ru-RU']").eaDoTracking({ "onclick": "return RioTracking.click( 300158407, '');" });

		    $("a[href*='http://windows.microsoft.com/ru-ru/windows-8/meet']").eaDoTracking({ "onclick": "return RioTracking.click( 300158408, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/crm.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158409, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-intune/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158410, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/erp.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158411, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158412, '');" });

		    $("a[href*='http://www.microsoft.com/sqlserver/ru/ru/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158413, '');" });

		    $("a[href*='http://microsoftstore.com/']").eaDoTracking({ "onclick": "return RioTracking.click( 300158414, '');" });
		    $("a[href*='http://pinpoint.microsoft.com/ru-ru/home']").eaDoTracking({ "onclick": "return RioTracking.click( 300158415, '');" });
		    $("a[href*='http://www.microsoft.com/ru-ru/partner/experts/']").eaDoTracking({ "onclick": "return RioTracking.click( 300158415, '');" });
		    $("a[href*='http://www.windowsazure.com/ru-ru/#buynow']").eaDoTracking({ "onclick": "return RioTracking.click( 300158416, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158417, '');" });
		    $("a[href*='http://technet.microsoft.com/ru-ru/evalcenter/hh699156.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158418, '');" });
		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158419, '');" });
		    /*	        $("a[href*='http://www.microsoft.com/en-ca/dynamics/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158419, '');" });*/
		    $("a[href*='http://www.microsoft.com/ru-ru/powerbi/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click( 300158420, '');" });
		    $("a[href*='http://office.microsoft.com/ru-ru/lync/FX103789571.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158425, '');" });
		    $("a[href*='https://onedrive.live.com/about/ru-ru/download/']").eaDoTracking({ "onclick": "return RioTracking.click(300158421, '');" });

		    $("a[href*='http://www.skype.com/en/']").eaDoTracking({ "onclick": "return RioTracking.click(300158423, '');" });

		    $("a[href*='http://windows.microsoft.com/ru-ru/windows/security-essentials-download/']").eaDoTracking({ "onclick": "return RioTracking.click(300158422, '');" });

		    $("a[href*='http://www.windowsazure.com/ru-ru/#tryit']").eaDoTracking({ "onclick": "return RioTracking.click(300158424, '');" });
		    $("a[href*='http://technet.microsoft.com/evalcenter']").eaDoTracking({ "onclick": "return RioTracking.click(300158626, '');" });
		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });


		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/productivity/") >= 0) {

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });


		}


		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/productivity-for-starting-a-business/") >= 0) {
		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='http://www.skype.com/ru/download-skype/skype-for-computer/']").eaDoTracking({ "onclick": "return RioTracking.click(300169887, '');" });

		    $("a[href*='http://windows.microsoft.com/ru-ru/internet-explorer/download-ie-MCM']").eaDoTracking({ "onclick": "return RioTracking.click(300169888, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });

		}


		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/productivity-growing-a-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}


		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/productivity-transforming-your-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/enterprise/products-and-technologies/windows-8/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/crm-purchase-online.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158409, '');" });

		    $("a[href*='http://www.windowsazure.com/ru-ru/pricing/free-trial/']").eaDoTracking({ "onclick": "return RioTracking.click(300169890, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}

		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/business-intelligence/") >= 0) {

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/business-intelligence-starting-a-business/") >= 0) {

		    $("a[href*='http://microsoftstore.com/']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='http://advertise.bingads.microsoft.com/']").eaDoTracking({ "onclick": "return RioTracking.click(300169892, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}


		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/business-intelligence-growing-a-business/") >= 0) {
		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });
		    $("a[href*='http://microsoftstore.com/']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/crm-purchase-online.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158409, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}


		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/business-intelligence-transforming-your-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/powerbi/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300169893, '');" });


		}

		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/business-intelligence-transforming-your-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://microsoftstore.com/']").eaDoTracking({ "onclick": "return RioTracking.click(300169408, '');" });



		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='http://www.microsoft.com/sqlserver/ru/ru/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158413, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/crm-purchase-online.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158409, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/mobility/") >= 0) {

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });

		}

		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/mobility-for-starting-a-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='http://www.skype.com/ru/download-skype/skype-for-computer/']").eaDoTracking({ "onclick": "return RioTracking.click(300169887, '');" });

            $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/mobility-growing-a-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });


		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}






		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/mobility-transforming-your-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-intune/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158410, '');" });

		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/enterprise/products-and-technologies/windows-8/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/dynamics/crm-purchase-online.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158409, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/scalability/") >= 0) {

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });

		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/scalability-for-starting-a-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });

		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/scalability-growing-a-business/") >= 0) {

		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/scalability-transforming-your-business/") >= 0) {
		    $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158412, '');" });

		    $("a[href*='http://www.microsoftstore.com/store/msmea/ru_RU/home/ThemeID.31924800/Currency.RUB/mktp.BY']").eaDoTracking({ "onclick": "return RioTracking.click(300169889, '');" });

		    $("a[href*='http://www.microsoft.com/ru-ru/windows/business/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158408, '');" });

		    $("a[href*='http://office.microsoft.com/ru-ru/FX102918419.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158407, '');" });

		    //$("a[href*='http://www.microsoft.com/en-ca/dynamics/crm.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158413, '');" });

		    $("a[href*='http://www.windowsazure.com/ru-ru/pricing/free-trial/']").eaDoTracking({ "onclick": "return RioTracking.click(300169890, '');" });

		    $("a[href*='http://www.microsoft.com/sqlserver/ru/ru/default.aspx']").eaDoTracking({ "onclick": "return RioTracking.click(300158413, '');" });

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });
		}



		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/government-technology-solutions/") >= 0) {

		    $("a[href*='https://sales.liveperson.net/hc/21661174/?cmd=file&file=visitorWantsToChat&site=21661174&byhref=1&SESSIONVAR!skill=EMEA.RU.CS.COM.PRESLS.SMB.Portal&imageUrl=https://sales.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a']").eaDoTracking({ "onclick": "return RioTracking.click(300158502, '');" });

		}

		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/modern-biz/") >= 0) {

		    $("a[href*='http://pinpoint.microsoft.com/ru-ru/home']").eaDoTracking({ "onclick": "return RioTracking.click(300148608, '');" });

		}


		if (eaUrl.toLowerCase().indexOf("ru-ru/business/modern-biz/digital-security-101/") >= 0) {

		    $("a[href*='http://www.inc.com/magazine/201312/john-brandon/hackers-target-small-business.html']").eaDoTracking({ "onclick": "return RioTracking.click(300192653, '');" });

		}

		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/modern-biz/get-ready-for-your-close-up/") >= 0) {

		    $("a[href*='https://business.twitter.com/photos-videos-content']").eaDoTracking({ "onclick": "return RioTracking.click(300192654, '');" });
		    $("a[href*='http://www.windowsphone.com/ru-ru/store/app/nokia-camera/bfd2d954-12da-415c-ad99-69a20f101e04']").eaDoTracking({ "onclick": "return RioTracking.click(300192655, '');" });
		    $("a[href*='http://www.windowsphone.com/ru-ru/store/app/proshot/3d6a3d7e-5aca-4aee-b059-590b9f53cc13']").eaDoTracking({ "onclick": "return RioTracking.click(300192656, '');" });
		    $("a[href*='http://www.windowsphone.com/ru-ru/store/app/adobe-photoshop-express/2ba43182-ba12-42c0-b503-158284f48bf8']").eaDoTracking({ "onclick": "return RioTracking.click(300192657, '');" });
		    $("a[href*='http://www.windowsphone.com/ru-ru/store/app/camera360/38397f3d-e54c-40d3-ae35-cebbaa23be1b']").eaDoTracking({ "onclick": "return RioTracking.click(300192658, '');" });
		    $("a[href*='http://www.windowsphone.com/ru-ru/store/app/photosynth/ef860a79-5f68-4ed6-aa21-c038d1a55517']").eaDoTracking({ "onclick": "return RioTracking.click(300192659, '');" });
		    $("a[href*='http://www.nokia.com/us-en/phones/phone/lumia1020/']").eaDoTracking({ "onclick": "return RioTracking.click(300192660, '');" });


		}
		
		if (eaUrl.toLowerCase().indexOf("/ru-ru/business/server-2003-end-of-support") >= 0) {
		
		$("a[href*='http://pinpoint.microsoft.com/ru-RU/companies/search/Серверы-хостинг-управление-сетью-b200017?q']").eaDoTracking({"onclick": "return RioTracking.click(300158415, '');"});
        $("a[href*='http://www.microsoft.com/ru-ru/server-cloud/products/windows-server-2012-r2/default.aspx']").eaDoTracking({"onclick": "return RioTracking.click(300158412, '');"});
        $("a[href*='http://www.azure.com/ru-ru/']").eaDoTracking({"onclick": "return RioTracking.click(300158416, '');"});
        
        }


	}
});